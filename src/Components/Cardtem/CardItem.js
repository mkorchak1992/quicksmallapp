import React from "react";
import "./CardItem.scss"

export const CardItem = (props) => {

    const {name, lastname, age, sex} = props;

    return (
        <ul className={"CardItem__container"}>
            <li className={"CardItem__container-item"}>name-{name}</li>
            <li className={"CardItem__container-item"}>lastname-{lastname}</li>
            <li className={"CardItem__container-item"}>age-{age}</li>
            <li className={"CardItem__container-item"}>sex-{sex}</li>
        </ul>
    )

}