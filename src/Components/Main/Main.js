import React, {useEffect, useState} from "react";
import {CardItem} from "../Cardtem/CardItem";
import "./Main.scss"

export const Main = () => {

    const [items, setItems] = useState(null)
    const [searchName, setSearchName] = useState('');
    const [searchLastName, setSearchLastName] = useState('');
    const [searchAge, setSearchAge] = useState('');
    const [male, setMale] = useState('')
    const [female, setFemale] = useState('')

    const maleFunc = (event) => {
        if (male === '') {
            setMale(event.target.value);
        } else {
            setMale('');
        }

    }
    const femaleFunc = (event) => {
        if (female === '') {
            setFemale(event.target.value);
        } else {
            setFemale('');
        }
    }


    useEffect(() => {
        fetch("https://venbest-test.herokuapp.com/.")
            .then(resp => resp.json())
            .then(data => setItems(data))
            .catch(err => alert('Something was wrong!Try to find mistake'))
    }, [])

    if (!items) {
        return (<p>Loading...</p>);
    }

    const filterArr = items.filter(item => {
        return (
            item.name.toLowerCase().includes(searchName.toLowerCase())
            && item.lastname.toLowerCase().includes(searchLastName.toLowerCase())
            && item.age.toString().includes(searchAge)
            && (item.sex.includes(male) && item.sex.includes(female))
        )
    })

    return (
        <>
            <h2 className={"search-form__title"}>Searching FORM</h2>
            <form className={"search-form__container"}>
                <input className={"search-form__container-field"} type={"text"} placeholder={"Search by Name"}
                       onChange={e => setSearchName(e.target.value)}/>
                <input className={"search-form__container-field"} type={"text"} placeholder={"Search by lastName"}
                       onChange={e => setSearchLastName(e.target.value)}/>
                <input className={"search-form__container-field"} type={"number"} placeholder={"Search by Age"}
                       onChange={e => setSearchAge(e.target.value)}/>

                <input type='checkbox' name="gender" value="m" onChange={maleFunc}/>M
                <input type='checkbox' name="gender" value="f" onChange={femaleFunc}/>F
            </form>
            <div className={"container"}>
                {
                    filterArr.map((el, index) =>
                        <CardItem
                            name={el.name}
                            lastname={el.lastname}
                            age={el.age}
                            sex={el.sex}
                            key={index}
                        />
                    )
                }
            </div>
        </>
    )
}